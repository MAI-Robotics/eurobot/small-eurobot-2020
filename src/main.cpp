#include <Arduino.h>
#include <math.h>
#include <Servo.h>

#define stepPinR 29 // needed right and step
#define stepPinL 28 //CLK

#define dirPinLeft 24 // direction
#define dirPinRight 25  //CW

#define enablePinR 22 //whether stepper is active or not, on LOW wheels are locked
#define enablePinL 22

//#define strongServo 3 //motors
#define boyoarmpin 7  //servo to spin boyos
#define pusharmspin 32  //servo for pushing arms
Servo boyoarm;
Servo pusharms;

#define trigger 6 // for all US sensors
#define echoR 5 //right US
#define echoL 3 //left US
#define echoF 2 //front US
#define echoB 4//back US//still to do in code below
/**** Some variables ****/
boolean boyoarmstatus;               //if true down else up
/**** Some constants ****/
const float wheelDiameter = 105;     // mm
const float outerWheelDistance = 21.5; // cm
const float wheelWidth = 50;         // mm
const float stepsPerRev = 1000;       // Steps that are needed to do one revolution
const float stopDistance = 20;       // distance to an obstacle the bot stops in front of in cm

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance * 10 - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * midWheelDistance) / distancePerStep); // °  only for in place turn 
const float tPerStep = 540.0;                                                   // minimal delay between the steps in µs
const float tPerStepTurn = 420.0; //I do not think we need that
/**
 * Measures the distance. without back does not work at time
 * @returns int the measured distance in cm
*/
/*int distanceMeasure()
{
  long durationF;
  long durationL;
  long durationR;
  int distance;
  Serial.print("distanceMeasure l50 distance ");
  Serial.println(distance);
  digitalWrite(trigger, LOW); // Clears the trigPin
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationF = pulseIn(echoF, HIGH); //front
  durationL = pulseIn(echoL, HIGH); //left
  durationR = pulseIn(echoR, HIGH); //right
  Serial.print("l62 durationF ");
  Serial.println(durationF);
  Serial.print("l65 durationL ");
  Serial.println(durationL);
  Serial.print("l67 durationR ");
  Serial.println(durationR);
  if(durationF==0 || durationL==0 || durationR==0){

  }
  // Calculating the distance
  if(durationF  < durationL && durationF < durationR){
    distance = durationF * 0.034 / 2;
  }else if(durationL  < durationR){
    distance = durationL * 0.034 / 2;
  }else{
    distance = durationR * 0.034 / 2;
  }
  Serial.println("line68 distanceMeasure()");
  Serial.println(distance);
  return distance;
}*/

/**
 * Measures the front distance 
 * @returns long the sound wave travel time in microseconds
*/
long durationMeasureF()
{
  digitalWrite(trigger, LOW); // Clears the trigPin
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  return pulseIn(echoF, HIGH);
}

/**
 * Measures the left distance 
 * @returns long the sound wave travel time in microseconds
*/
long durationMeasureL()
{
  digitalWrite(trigger, LOW); // Clears the trigPin
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  return pulseIn(echoL, HIGH);
}

/**
 * Measures the right distance 
 * @returns long the sound wave travel time in microseconds
*/
long durationMeasureR()
{
  digitalWrite(trigger, LOW); // Clears the trigPin
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  return pulseIn(echoR, HIGH);
}

/**
 * Measures the back distance 
 * @returns int the measured distance in cm
*/
bool findObjectB()
{
  long durationB;
  
  /*Serial.print("l146 no back distance ");
  Serial.println(distance);*/

  digitalWrite(trigger, LOW); // Clears the trigPin
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  
  durationB = pulseIn(echoB, HIGH); // Reads the echoPin, returns the sound wave travel time in microseconds
  Serial.print("durationMeasure l158 durationB ");
  Serial.println(durationB);  //incorrect lines

  int distanceB=durationB * 0.034 / 2; // Calculating the distance in cm
  Serial.print("distanceMeasure l162 distanceB ");
  Serial.println(distanceB);

  if(distanceB<stopDistance){
    return true;  //returns true if distance is fine
  }else{
    return false; //returns false if distance is to short
  }
}
/*checks the distance to an obstacle and returns boolean if distance is fine*///shouldn't it be if distance is to short
bool findObjectF(){
  //Serial.println("Überbrückung");
  //return false; //until driveFast works properly skips US control
  int sdistance;  //shotest distance
  int durationF = durationMeasureF(); //gets front durations
  int durationL = durationMeasureL();
  int durationR = durationMeasureR();
  Serial.print("findObject l175 durationF: ");
  Serial.println(durationF);
  Serial.print("findObject l177 durationL: ");
  Serial.println(durationL);
  Serial.print("findObject l179 durationR: ");
  Serial.println(durationR);

  if(durationF!=0 && durationL!=0 && durationR!=0){
    // Calculating the distance
    if( durationF < durationL && durationF < durationR){
      sdistance = durationF * 0.034 / 2;
    }else if(durationL  < durationR){
      sdistance = durationL * 0.034 / 2;
    }else{
      sdistance = durationR * 0.034 / 2;
    }
  }else if(durationL!=0 && durationR!=0){ //if only durationF==0
    Serial.println("l180 Front sensor error");
    if(durationL  < durationR){
      sdistance = durationL * 0.034 / 2;
    }else{
      sdistance = durationR * 0.034 / 2;
    }
  }else if(durationL!=0 && durationF!=0){  //if only durationR==0
    Serial.println("l187 Right sensor error");
    if(durationL  < durationF){
        sdistance = durationL * 0.034 / 2;
    }else{
        sdistance = durationF * 0.034 / 2;
    }
  }else if(durationR!=0 && durationF!=0){  //if only durationL==0
    Serial.println("l194 Left sensor error");
    if(durationL  < durationF){
        sdistance = durationL * 0.034 / 2;
    }else{
        sdistance = durationF * 0.034 / 2;
    }
  }else{  //if too many errors
    Serial.println("l201 many sensor errors");
    sdistance = 0;
  }

  if(sdistance>stopDistance){         //true means distance is still fine
    Serial.println("No object reachable");
    return false;  //returns true if distance is fine
  }else
  {
    Serial.println("object reachable");
    return true; //returns false if distance is to short
  }
}
/*void turnFast(unsigned int steps, bool dir) 
  {
     delay(10);
     digitalWrite(dirPinLeft, dir ? 1 : 0);
     digitalWrite(dirPinRight, dir ? 1 : 0);
     digitalWrite(enablePin, LOW);
     delay(10);
     float a = 0; //for acceleration  
     for (unsigned int i = 0; i < steps; i++)
        {
           a = 0;
           if (i < 400 && i <= steps / 2)
                {
                   a = tPerStep * 0.0025 * (400 - i);
                }
                if (steps - i < 400 && i > steps / 2)
                {
                  a = tPerStep * 0.0025 * (400 - (steps - i));
                }
                if (/*digitalRead(IR) &&*/
/* steps - i > 200)
                {
                  for (int blahh = 0; blahh < 100; blahh++)
                  {
                    a = tPerStep * 0.0025 * (100 - blahh);
                    digitalWrite(stepPin, HIGH);
                    delayMicroseconds(tPerStep + a);
                    digitalWrite(stepPin, LOW);
                    delayMicroseconds(tPerStep + a);
                  }
                  digitalWrite(enablePin, HIGH);
                  /*while (digitalRead(US))
                  {
                    delay(500);     //alternetive needed
                  }*/
/*
                  digitalWrite(enablePin, LOW);
                  for (int b = 0; b < 100; b++)
                  {
                    a = tPerStep * 0.0025 * (100 - b);
                    digitalWrite(stepPin, HIGH);
                    delayMicroseconds(tPerStep + a);
                    digitalWrite(stepPin, LOW);
                    delayMicroseconds(tPerStep + a);
                  }
                  i += 200;
                }
                digitalWrite(stepPin, HIGH);
                delayMicroseconds(tPerStep + a);
                digitalWrite(stepPin, LOW);
                delayMicroseconds(tPerStep + a);
        }
        digitalWrite(enablePin, HIGH);
  }*/

/**
 * Method to choose collishion avoidance
 * @param dir direction is true so forward use find Object in front otherwise in back
 * @param CoA if collishion avoidance is active
 * returns boolean if object or error was found
 * */
bool chooseCoA(bool dir,bool CoA){
  if(CoA){
    if(dir){
      return findObjectF();
    }else{
      return findObjectB();
    }
  }else{
    //Serial.println("l299 choose CoA fahre ohne hinderniserkennung");
    return false;
  }
}
/**
 * drives both motors
 * @param steps steps to drive
 * @param dir direction to drive true forward || false backward
 * @param CoA drive with collision avoidance true || false not implemented
 * */
void driveFast(unsigned int steps, bool dir,bool CoA)  //add COA
{
  // Set direction. If correctly wired, opposite values should mean driving foreward
  //digitalWrite(dirPinLeft, dir);
  //digitalWrite(dirPinRight, !dir);
  if(dir){  
    digitalWrite(dirPinLeft, LOW);  //LOW forward || HIGH backward
    digitalWrite(dirPinRight, HIGH);//LOW backward || HIGH forward
  }else{
    digitalWrite(dirPinLeft, HIGH);
    digitalWrite(dirPinRight, LOW);
  }
  //    L    R
  //    H    H  rückwärts
  //    H    L  ggdUhrzeigersinn
  //    L    H  imUhrzeigersinn
  //    L    L  vorwärts

  // Enable steppers so they accept commands
  digitalWrite(enablePinR, LOW);
  digitalWrite(enablePinL, LOW);

  delay(10);   // wait for everything to settle

  float a = 0; // for acceleration, will be added to normal step time later

  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;

    if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
      a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
    }

    if (steps - i < 300 && i > steps / 2) // decelerate to make stopping smoother
    {
      a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
    }

    // place the detection for obstacles here PLZ create mehtod
    if (/*digitalRead(IR)*/chooseCoA(dir,CoA) && steps - i > 200) // happens everytime without near end
    {

      for (int b = 0; b < 100; b++)         // Break using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration gets lower every time to brake efficiently

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse
        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse
        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      digitalWrite(enablePinR, HIGH);       // Disable steppers during the stop phase
      digitalWrite(enablePinL, HIGH);       // So if we get pushed away we don't block

      /*  You need this to stay blocked if obstacle does not move*/
      while (chooseCoA(dir,CoA))
      {
        delay(500); 
      }

      digitalWrite(enablePinR, LOW);        // Enable steppers again to be able to drive again
      digitalWrite(enablePinL, LOW);

      for (int b = 0; b < 100; b++)         // Speed up again using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration increases every step to get to speed again

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse
        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse
        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse
        
        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      i += 1;//200;                             // Add 200 steps so that we don't drive too much
    }

    digitalWrite(stepPinR, HIGH);           // Pull both pins high to start pulse
    digitalWrite(stepPinL, HIGH);           // Pull both pins high to start pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time

    digitalWrite(stepPinR, LOW);            // Pull both pins low to stop pulse
    digitalWrite(stepPinL, LOW);            // Pull both pins low to stop pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time
  }
  digitalWrite(enablePinR, HIGH);           // Disable steppers again
  digitalWrite(enablePinL, HIGH);
}

/* Method to drive left stepper */
void driveFastL(unsigned int steps, bool dir,bool CoA)
{
  // Set direction. If correctly wired, opposite values should mean driving foreward
  digitalWrite(dirPinLeft, dir);

  // Enable steppers so they accept commands
  digitalWrite(enablePinL, LOW);

  delay(10);   // wait for everything to settle

  float a = 0; // for acceleration, will be added to normal step time later

  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;

    if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
      a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
    }

    if (steps - i < 300 && i > steps / 2) // decelerate to make stopping smoother
    {
      a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
    }

    // place the detection for obstacles here PLZ create mehtod
    if (/*digitalRead(IR)*/chooseCoA(dir,CoA) && steps - i > 200) // happens everytime without near end
    {

      for (int b = 0; b < 100; b++)         // Break using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration gets lower every time to brake efficiently

        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      digitalWrite(enablePinL, HIGH);       // So if we get pushed away we don't block

      /*  You need this to stay blocked if obstacle does not move*/
      while (chooseCoA(dir,CoA))
      {
        delay(500); 
      }

      digitalWrite(enablePinL, LOW);

      for (int b = 0; b < 100; b++)         // Speed up again using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration increases every step to get to speed again

        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse
        
        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      i += 200;                             // Add 200 steps so that we don't drive too much
    }

    digitalWrite(stepPinL, HIGH);           // Pull both pins high to start pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time

    digitalWrite(stepPinL, LOW);            // Pull both pins low to stop pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time
  }
  digitalWrite(enablePinL, HIGH);
}
/* Method to drive right stepper motor*/
void driveFastR(unsigned int steps, bool dir,bool CoA)
{
  // Set direction. If correctly wired, opposite values should mean driving foreward
  digitalWrite(dirPinRight, dir);

  // Enable steppers so they accept commands
  digitalWrite(enablePinR, LOW);

  delay(10);   // wait for everything to settle

  float a = 0; // for acceleration, will be added to normal step time later

  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;

    if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
      a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
    }

    if (steps - i < 300 && i > steps / 2) // decelerate to make stopping smoother
    {
      a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
    }

    // place the detection for obstacles here PLZ create mehtod
    if (/*digitalRead(IR)*/chooseCoA(dir,CoA) && steps - i > 200) // happens everytime without near end
    {

      for (int b = 0; b < 100; b++)         // Break using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration gets lower every time to brake efficiently

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      digitalWrite(enablePinR, HIGH);       // Disable steppers during the stop phase

      /*  You need this to stay blocked if obstacle does not move*/
      while (chooseCoA(dir,CoA))
      {
        delay(500); 
      }

      digitalWrite(enablePinR, LOW);        // Enable steppers again to be able to drive again

      for (int b = 0; b < 100; b++)         // Speed up again using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration increases every step to get to speed again

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse
        
        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      i += 200;                             // Add 200 steps so that we don't drive too much
    }

    digitalWrite(stepPinR, HIGH);           // Pull both pins high to start pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time

    digitalWrite(stepPinR, LOW);            // Pull both pins low to stop pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time
  }
  digitalWrite(enablePinR, HIGH);           // Disable steppers again
}

/*
float drive(float distance, bool dir, bool useCoA = true)
{
  int stepsDone = 0;
  int stepsToDo = distance / distancePerStep;
  if (useCoA)
  {
    printf("I will do %d steps with CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!d%5d\n", dir ? stepsToDo : -stepsToDo);
      wait_ms(20);
      while (!feedback)
      {
        while (((distFL <= stopDist && dir) ||
             (distFR <= stopDist && dir) ||
             (distBL <= stopDist && !dir) ||
             (distBR <= stopDist && !dir)))
        {
          interruptDrive = 1;
          printf("Obstacle: %d, %d, %d, %d\n", distFL, distFR, distBL, distBR);
        }
        interruptDrive = 0;
      }
    }
  }
  else
  {
    printf("I will do %d steps without CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!d%5d\n", dir ? stepsToDo : -stepsToDo);

      wait_ms(stepsToDo * tPerStep / 1000 / 2);
      while (!feedback)
      {
        wait_ms(5);
      }
    }
  }

  return stepsDone * distancePerStep;
}*/
/*
 * Method to turn the bot around a circle
 * @param angle   Angle which the bot has to turn, negative angle means turn backwards
 * @param radius  distance from the center of the bot to the centre of the circle the bot turns around
 * @param dir     Direction in wich the bot will turn true: left | false: right
 * @param useCoA  Wether to use collision avoidance or not
 * incomplete
*/
void turn(float angle, float radius, bool dir, bool CoA){
  
  bool directionFB=true; //forward, backward direction
  if(angle<0){
      directionFB=false;  // f/w direction for the motor to drive
      angle = angle * -1; //sets step numbers positive
  }

  if(radius<0){
    radius=radius*(-1); //to make sure the distance computing will not be disheveled
  }

  float s1 = 2 * PI * (radius - 0.5 * midWheelDistance) * angle / 360; //distance of the inner circle wheel
  float s2 = 2 * PI * (radius + 0.5 * midWheelDistance) * angle / 360; //distance of the outer circle wheel
  float n1 = s1 / distancePerStep;  //number of steps to drive distance
  float n2 = s2 / distancePerStep;  //number of steps to drive distance
  int intRelation; //relation of step numbers
  float relation; //relation of step numbers
  float reststeps;

  
    if(dir){  //n1=left steps
      relation=n1/n2; //relation of step numbers
      intRelation=(int) n1/n2; //relation of step numbers
      reststeps=(relation-intRelation)*n1;
      float extrastep=n2/reststeps;//alle wieviele steps ein extrastep kommt
      int intextrastep= (int) extrastep;
      int counter1=0;
      extrastep = n2/(extrastep-intextrastep)*n1;
      intextrastep= (int) extrastep;
      if(relation-intRelation>=0.5){  //if the relations' first decimal is greater than 5
        for(float s=0; s<0.5*n1; s++){
          driveFastL(1,directionFB,CoA);
          driveFastR(1*(intRelation+1),directionFB,CoA); //in the first half one step more will be done
          delayMicroseconds(tPerStep*intRelation);  //for the left motor to wait for the right => normally not needed
        }
        for(int u=0; u<0.5*n1; u++){
          driveFastL(1,directionFB,CoA);
          driveFastR(1*intRelation,directionFB,CoA);  //in the second half the normal step relation will be done
          delayMicroseconds(tPerStep*intRelation);  //for the left motor to wait for the right => normally not needed
        }
        driveFastR(n2-((intRelation+1)*n1*0.5+intRelation*0.5*n1),directionFB,CoA); //rest steps
      }
      else{
        for(int t=0; t<n1; t++){
          driveFastL(1,directionFB,CoA);
          driveFastR(1*intRelation,directionFB,CoA);  //otherwise drive in normal relation
          delayMicroseconds(tPerStep*intRelation);  //for the left motor to wait for the right => normally not needed
        }
        driveFastR(n2-n1*intRelation,directionFB,CoA);  //rest steps
      }
    }
    else{ //n1=right steps
      relation=n2/n1; //relation of step numbers
      intRelation=n2/n1; //relation of step numbers
      if(relation-intRelation>=0.5){  //if the relations' first decimal is greater than 5
        for(float s=0; s<0.5*n1; s++){
          driveFastR(1,directionFB,CoA);
          driveFastL(1*(intRelation+1),directionFB,CoA); //in the first half one step more will be done
          delayMicroseconds(tPerStep*intRelation);  //for the right motor to wait for the left => normally not needed
        }
        for(int u=0; u<0.5*n1; u++){
          driveFastR(1,directionFB,CoA);
          driveFastL(1*intRelation,directionFB,CoA);  //in the second half the normal step relation will be done
          delayMicroseconds(tPerStep*intRelation);  //for the right motor to wait for the left => normally not needed
        }
        driveFastR(n2-((intRelation+1)*n1*0.5+intRelation*0.5*n1),directionFB,CoA); // rest steps
      }
      else{
        for(int t=0; t<n1; t++){
          driveFastR(1,directionFB,CoA);
          driveFastL(1*intRelation,directionFB,CoA);  //otherwise drive normal rellation
          delayMicroseconds(tPerStep*intRelation);  //for the right motor to wait for the left => normally not needed
        }
        driveFastL(n2-n1*intRelation,directionFB,CoA);  //do rest steps
      }
    }
  
}

/**
 * Method to open front arms
 */
void openArms(){
  pusharms.write(0);  //90 closed; 0 or 180 opened; 0 optimum
}

/*Method to drop or pull boyoarm to the right, ALWAYS PUT ARM AFTER USEED UP AGAIN*/
void dropOrPullboyoarmR(){
  if(boyoarmstatus){    //if arm is down
    boyoarm.write(90);  //pull up arm
    Serial.println("l701 arm up");
    boyoarmstatus=false;
    Serial.print("l703 boyoarmstatus= ");
    Serial.println(boyoarmstatus);
  }else{                //if arm is up
    boyoarm.write(180); //drop arm to right side
    Serial.println("l707 arm R");
    boyoarmstatus=true;
    Serial.print("l709 boyoarmstatus= ");
    Serial.println(boyoarmstatus);
  }
}

  /*Method to drop or pull boyoarm to the left ALWAYS PUT ARM AFTER USEED UP AGAIN*/
void dropOrPullboyoarmL(){
  if(boyoarmstatus){    //if arm is down
    boyoarm.write(90);  //pull up arm
    Serial.println("l739 arm up");
    boyoarmstatus=false;
    Serial.print("l738 boyoarmstatus= ");
    Serial.println(boyoarmstatus);
  }else{                //if arm is up
    boyoarm.write(0); //drop arm to left side
    Serial.println("l744 arm Left");
    boyoarmstatus=true;
    Serial.print("l744 boyoarmstatus= ");
    Serial.println(boyoarmstatus);
  }
  
}

/**
 * Method to turn the bot in place 
 * @param angle   Angle which the bot has to turn;
 * @param dir     Direction in wich the bot will turn true: left | false: right
 * @param useCoA  Wether to use collision avoidance or not, yet not
*/
void turn(float angle, bool dir, bool CoA){

  /*if(angle<0){
    angle=360-angle;  //cirles only in one direction
  }
  
  int a = (int) angle/anglePerStep; //steps

  if(dir){  //left turn
    for(int stepcount=0;stepcount<angle/anglePerStep;stepcount++){
      driveFastL(1,!dir,CoA); //left motor backward
      driveFastR(1,dir,CoA);  //right motor forward
    }
    //driveFastL(abs(a),!dir,CoA); //left motor backward
    //driveFastR(abs(a),dir,CoA);  //right motor forward
  }
  else{ //right turn
    for(int stepcount=0;stepcount<angle/anglePerStep;stepcount++){
      driveFastL(1,dir,CoA);  //left motor forward
      driveFastR(1,!dir,CoA); //right motor backwards
    }
    //driveFastL(abs(a),dir,CoA); //left motor backward
    //driveFastR(abs(a),!dir,CoA);  //right motor forward
  }*/
  /*
  float aps=360/stepsPerRev;
  int steps=(int) angle/aps; //calculate number of steps to do
  steps=abs(steps);                   //Calculates the absolute value of a number.
  if(dir){
    digitalWrite(dirPinLeft,LOW);
    digitalWrite(dirPinRight,HIGH);
  }else{                              //LOW forward || HIGH backward
    digitalWrite(dirPinLeft,HIGH);
    digitalWrite(dirPinRight,LOW);
  }

  // Enable steppers so they accept commands
  digitalWrite(enablePinR, LOW);
  digitalWrite(enablePinL, LOW);

  delay(10);   // wait for everything to settle

  float a = 0; // for acceleration, will be added to normal step time later

  for (unsigned int i = 0; i < steps; i++)
  {
    a = 0;

    if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
      a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
    }

    if (steps - i < 300 && i > steps / 2) // decelerate to make stopping smoother
    {
      a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
    }

    // place the detection for obstacles here PLZ create mehtod
    if (/*digitalRead(IR) oder chooseCoA(dir,CoA) && steps - i > 200) // happens everytime without near end
    {

      for (int b = 0; b < 100; b++)         // Break using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration gets lower every time to brake efficiently

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse
        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse
        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      digitalWrite(enablePinR, HIGH);       // Disable steppers during the stop phase
      digitalWrite(enablePinL, HIGH);       // So if we get pushed away we don't block

      //  You need this to stay blocked if obstacle does not move
      while (chooseCoA(dir,CoA))
      {
        delay(500); 
      }

      digitalWrite(enablePinR, LOW);        // Enable steppers again to be able to drive again
      digitalWrite(enablePinL, LOW);

      for (int b = 0; b < 100; b++)         // Speed up again using 100 steps
      {
        a = tPerStep * 0.0025 * (100 - b);  // acceleration increases every step to get to speed again

        digitalWrite(stepPinR, HIGH);       // Pull both pins high to start pulse
        digitalWrite(stepPinL, HIGH);       // Pull both pins high to start pulse

        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time

        digitalWrite(stepPinR, LOW);        // Pull both pins low to stop pulse
        digitalWrite(stepPinL, LOW);        // Pull both pins low to stop pulse
        
        delayMicroseconds(tPerStep + a);    // wait the given time including acceleration time
      }

      i += 1;//200;                             // Add 200 steps so that we don't drive too much
    }

    digitalWrite(stepPinR, HIGH);           // Pull both pins high to start pulse
    digitalWrite(stepPinL, HIGH);           // Pull both pins high to start pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time

    digitalWrite(stepPinR, LOW);            // Pull both pins low to stop pulse
    digitalWrite(stepPinL, LOW);            // Pull both pins low to stop pulse

    delayMicroseconds(tPerStep + a);        // wait the given time including acceleration time
  }
  digitalWrite(enablePinR, HIGH);           // Disable steppers again
  digitalWrite(enablePinL, HIGH);
*/}

/**
*Methode to drive the bot
* @param distance Distance to drive in mm
* @param dir direction to drive true forward || false backward
* @param CoA if collision avoidance should be active
*/
void drive(float distance, boolean dir,bool CoA){
  float phi=abs(distance/distancePerStep);
  driveFast((int) phi,dir,CoA);
}
/**   for Tobi
 * Method to turn the bot
 * @param distance  Distance which the bot has to drive. Measured in mm
 * @param dir       Direction in wich the bot will turn true: forward | false: backward
 * 
 * @returns float   Time in wich the drive will be done
*/



/*
 * Method to turn the bot around a circle
 * @param angle   Angle which the bot has to turn, negative angle means turn backwards
 * @param radius  distance from the center of the bot to the centre of the circle the bot turns around in mm
 * @param dir     Direction in wich the bot will turn true: left | false: right
 * @param useCoA  Wether to use collision avoidance or not
 * wrong structure
*/
void curve(float angle, float radius, bool dir, bool CoA){
  Serial.println("l 910 Drehung entlang einer Kreisbahn");
  
  if(angle>0){  //drive bot forward
    Serial.print("Drehung vorwärts");
    digitalWrite(dirPinLeft,LOW);
    digitalWrite(dirPinRight,HIGH);
  }else{        //drive bot backward
    Serial.print("Drehung rückwärts");
    angle = angle * -1; //sets step numbers positive
    digitalWrite(dirPinLeft,HIGH);
    digitalWrite(dirPinRight,LOW);
  }
  if(radius<0){
    abs(radius); //to make sure the distance computing will not be disheveled
  }
  /*if(dir){                  //if left turn
    #define stepPinI 28;    //left wheel is inner wheel
    #define stepPinO 29;    //right wheel is outer wheel
  }else{
    #define stepPinI 29;
    #define stepPinO 28;
  }*/
  //calculation of the distance has been checked and is fine
  float s1 = 2 * PI * (radius - 0.5 * midWheelDistance) * angle / 360; //distance of the inner circle wheel
  float s2 = 2 * PI * (radius + 0.5 * midWheelDistance) * angle / 360; //distance of the outer circle wheel
  float n1 = s1 / distancePerStep;  //number of steps to drive inner distance
  float n2 = s2 / distancePerStep;  //number of steps to drive outer distance
  int intRelation =(int) n1/n2;//relation of step numbers
  float relation =n1/n2; //relation of step numbers
  float reststeps =(relation-intRelation)*n1;
  float extrastep=n2/reststeps;//alle wieviele steps ein extrastep kommt
  int intextrastep1= (int) extrastep;
  float extrastep2 = n2/(extrastep-intextrastep1)*n1;
  int intextrastep2= (int) extrastep2;
  /*
  //das folgende müsste in die for schleife
  if(i==intextrastep1 && i==intextrastep2){
    //zwei schritte außen
  }else if(i==intextrastep1 || i==intextrastep2){
    //ein schritt außen
  }*/

  /*digitalWrite(enablePinL, LOW);
  digitalWrite(enablePinR, LOW);
  if(dir){
    Serial.println("Drehung links");
    for (unsigned int i = 0; i < n2; i++){
        if(maybeStep(i,n1,n2)){
        stepSmaller();//austauschen
      }
      stepLarger();
    }
    }*/

  /*different number used*/const unsigned int aOverSteps = 1490; //1000 steps are one revolution
  float a1 = 0; //for inner acceleration
  float a2 = 0; //for outer acceleration
  digitalWrite(enablePinL, LOW);
  digitalWrite(enablePinR, LOW);
  if(dir){
    Serial.println(", links");
    for (unsigned int i = 0; i < n1; i++){

      a1 = 0;
      if (i < aOverSteps && i <= n1 / 2) {   //first half of steps
        a1 = (tPerStepTurn / aOverSteps) * 2.0 * (aOverSteps - i); //a is long at beginning and getting shorter
      }
      if (n1 - i < aOverSteps && i > n1 / 2) {  //second half of steps
        a1 = (tPerStepTurn / aOverSteps) * 2.3 * (aOverSteps - (n1 - i)); //a is short at beginning and getting longer, in everage 2.3/2 longer than in the first half
      }

      a2=0;
      if (i < aOverSteps && i <= n2 / 2) {   //first half of steps
        a2 = (tPerStepTurn / aOverSteps) * 2.0 * (aOverSteps - i); //a is long at beginning and getting shorter
      }
      if (n2 - i < aOverSteps && i > n2 / 2) {  //second half of steps
        a2 = (tPerStepTurn / aOverSteps) * 2.3 * (aOverSteps - (n2 - i)); //a is short at beginning and getting longer, in everage 2.3/2 longer than in the first half
      }
      float a3=a2-a1; //claculates the left time after a1 is over for a2

      digitalWrite(stepPinL, HIGH);
      delayMicroseconds(tPerStepTurn + a1);
      digitalWrite(stepPinR, HIGH);
      delayMicroseconds(a3);
      digitalWrite(stepPinL, LOW);
      delayMicroseconds(tPerStepTurn + a1);
      digitalWrite(stepPinR, LOW);
      delayMicroseconds(a3);
    }
  }else{
    Serial.println(", rechts");
    for (unsigned int i = 0; i < n1; i++){

      a1 = 0;
      if (i < aOverSteps && i <= n1 / 2) {   //first half of steps
        a1 = (tPerStepTurn / aOverSteps) * 2.0 * (aOverSteps - i); //a is long at beginning and getting shorter
      }
      if (n1 - i < aOverSteps && i > n1 / 2) {  //second half of steps
        a1 = (tPerStepTurn / aOverSteps) * 2.3 * (aOverSteps - (n1 - i)); //a is short at beginning and getting longer, in average 2.3/2 longer than in the first half
      }

      a2=0;
      if (i < aOverSteps && i <= n2 / 2) {   //first half of steps
        a2 = (tPerStepTurn / aOverSteps) * 2.0 * (aOverSteps - i); //a is long at beginning and getting shorter
      }
      if (n2 - i < aOverSteps && i > n2 / 2) {  //second half of steps
        a2 = (tPerStepTurn / aOverSteps) * 2.3 * (aOverSteps - (n2 - i)); //a is short at beginning and getting longer, in average 2.3/2 longer than in the first half
      }
      float a3=a2-a1; //claculates the left time after a1 is over for a2

      digitalWrite(stepPinR, HIGH);
      delayMicroseconds(tPerStepTurn + a1);
      digitalWrite(stepPinL, HIGH);
      delayMicroseconds(a3);
      digitalWrite(stepPinR, LOW);
      delayMicroseconds(tPerStepTurn + a1);
      digitalWrite(stepPinL, LOW);
      delayMicroseconds(a3);
    }
  }
  digitalWrite(enablePinR, HIGH);
  digitalWrite(enablePinL, HIGH);

  delay(100);
  Serial.println("Drehung Ende");
}


/*in place turn*/
void turnWoCoA(short angle) {//postitive angle for clockwise rotation
  
  unsigned int steps = abs(angle) / anglePerStep;
  bool pol = angle > 0; //polition TRUE means turn left
  if (!pol) steps *= 1.083;
  if (pol) steps *= 1.097;
  Serial.println(steps);

  digitalWrite(enablePinL, LOW);
  digitalWrite(enablePinR, LOW);
  if(pol){
    digitalWrite(dirPinLeft,HIGH);
    digitalWrite(dirPinRight,HIGH);
  }else{
    digitalWrite(dirPinLeft,LOW);
    digitalWrite(dirPinRight,LOW);
  }

  delay(10);

  const unsigned int aOverSteps = 1490; //1000 steps are one revolution

  float a = 0; //for acceleration
  for (unsigned int i = 0; i < steps; i++) {  //until steps are done
    a = 0;
    if (i < aOverSteps && i <= steps / 2) {   //first half of steps
      a = (tPerStepTurn / aOverSteps) * 2.0 * (aOverSteps - i); //a is long at beginning and getting shorter
    }
    if (steps - i < aOverSteps && i > steps / 2) {  //second half of steps
      a = (tPerStepTurn / aOverSteps) * 2.3 * (aOverSteps - (steps - i)); //a is short at beginning and getting longer, im Schnitt 2.3/2 longer than in the first half
    }

    digitalWrite(stepPinL, HIGH);
    digitalWrite(stepPinR , HIGH);
    delayMicroseconds(tPerStepTurn + a);
    digitalWrite(stepPinL , LOW);
    digitalWrite(stepPinR , LOW);
    delayMicroseconds(tPerStepTurn + a);
  }
  digitalWrite(enablePinR, HIGH);
  digitalWrite(enablePinL, HIGH);

  delay(100);
  }

/**
 * Method to do a step on given pin
 * @param pin Pin which executes one impulse
 * @param a accelerationtime
 */
void step(int pin,float a){
  digitalWrite(pin, HIGH);
  delayMicroseconds(tPerStep+a);
  digitalWrite(pin, LOW);
  delayMicroseconds(tPerStep+a);
}

/**
 * Method to check if step on other wheel is necessary (for curve)
 * @param i current step
 */
bool maybeStep(int i,int relation){
  /*int x = i--;  //tells last step (while first cycle negative)
  int y = relation;*/
  return (i-- < relation)&&(relation < i);  //returns true if relation from bigger to smaller step number is bigger than the last step and the relation is smaller than the current step
}

void curve(float angle, float radius,bool CoA){ //nur positive Werte führen zu einer linkskurve vorwärts

  float distInnerWheel = 2 * PI * (radius - 0.5 * midWheelDistance) * (angle / 360); 
  float distOuterWheel = 2 * PI * (radius + 0.5 * midWheelDistance) * (angle / 360);
  float stepsInnerWheel = distInnerWheel / distancePerStep;
  float stepsOuterWheel = distOuterWheel / distancePerStep;
  int relation=stepsOuterWheel/stepsInnerWheel; //relation large to small

  bool rightCurve = (angle * radius) < 0;
  int outerdirPin = rightCurve ? dirPinLeft : dirPinRight;
  int innerdirPin = rightCurve ? dirPinRight : dirPinLeft;
  
  digitalWrite(enablePinR, LOW);
  digitalWrite(enablePinL, LOW);
  for(int i = 0; i < stepsOuterWheel; i++){
    if(maybeStep(i,relation)){
      step(innerdirPin,0);  //hier wäre innerdirPin richtig
    }
    step(outerdirPin,0);  //hier wäre outerdirPin richtig
  }

  digitalWrite(enablePinR, HIGH);
  digitalWrite(enablePinL, HIGH);
}

void setup()
{
  pinMode(echoF, INPUT);  //set echopins as input
  pinMode(echoR, INPUT);
  pinMode(echoL, INPUT);
  pinMode(dirPinLeft, OUTPUT); //set dirPin as output
  pinMode(dirPinRight, OUTPUT);
  pinMode(trigger, OUTPUT); //set trigger for US as output
  pinMode(enablePinR, OUTPUT);  //locks the motors
  pinMode(enablePinL, OUTPUT);
  pinMode(stepPinR, OUTPUT);
  pinMode(stepPinL, OUTPUT);
  pinMode(boyoarmpin, OUTPUT);
  pinMode(pusharmspin, OUTPUT);
  boyoarm.attach(7);
  pusharms.attach(32);
  //digitalWrite(dirPinLeft,LOW);
  //digitalWrite(dirPinRight,LOW);
  digitalWrite(trigger,LOW);
  digitalWrite(enablePinR, LOW);  //locks the motors
  digitalWrite(enablePinL, LOW);
  digitalWrite(stepPinR, LOW);
  digitalWrite(stepPinL, LOW);
  //digitalWrite(boyoarm, LOW);
  //digitalWrite(pusharms, LOW);
  Serial.begin(9600); //start serial communication
  //while(!Serial) {} // Warten bis die serielle Schnittstelle antwortet.
}

void loop()
{
  Serial.println("loop beginn");

  /*delay(1000);
  Serial.println("delay over");*/

  //Serial.print("boyoarm: ");
  boyoarm.write(180); //180 right; 90 mid; left 0
  delay(100);
  boyoarm.write(0); //180 right; 90 mid; left 0
  //openArms();
  //Serial.print("pusharms: ");
  //pusharms.write(90);  //90 closed; 0 oder 180 opened; 0 optimum
  drive(1000,true,false);
  //driveFast(10000,true,false);
  //turn(180,true,false);
  //turn(180,1000,false,false,false);
  //turnWoCoA(90);
  //curve(90,100,false);
  //curve(90,100,false,false);
  Serial.println("loop end");
  delay(600000);//delay(ms) here 10 min
}